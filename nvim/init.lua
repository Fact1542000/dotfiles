require('settings')

require('mappings')

require('colorschemes-config.nightfox')

require('packer-config')

require('nvim-tree-config')

require('lualine-config')

require('barbar-config')

require('treesitter-config')

require('dashboard-config')

-- Lsp zero
require('lsp-config.lsp-zero-config')
require('lsp-config.masom-config')
