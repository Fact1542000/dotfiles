return require'packer'.startup(function()
    -- Packer
    use 'wbthomason/packer.nvim'

    -- Themes
    use "EdenEast/nightfox.nvim"

    -- Nvimtree
    use 'kyazdani42/nvim-web-devicons'
    use 'kyazdani42/nvim-tree.lua'

    -- Notify
    use 'rcarriga/nvim-notify'

    -- Lualine
    use 'nvim-lualine/lualine.nvim'

    -- barbar
    use 'romgrk/barbar.nvim'

    -- Tree-sitter
    use { 'nvim-treesitter/nvim-treesitter', run = ':TSUpdate' }
    use 'nvim-treesitter/nvim-treesitter-refactor'

    -- Telescope
    use { 
        'nvim-telescope/telescope.nvim', 
        requires = { {'nvim-lua/plenary.nvim'} }
    }

    -- Lsp zero
    use {
        'VonHeikemen/lsp-zero.nvim',
            requires = {
              -- LSP Support
              {'neovim/nvim-lspconfig'},
              {'williamboman/mason.nvim'},
              {'williamboman/mason-lspconfig.nvim'},

              -- Autocompletion
              {'hrsh7th/nvim-cmp'},
              {'hrsh7th/cmp-buffer'},
              {'hrsh7th/cmp-path'},
              {'saadparwaiz1/cmp_luasnip'},
              {'hrsh7th/cmp-nvim-lsp'},
              {'hrsh7th/cmp-nvim-lua'},

              -- Snippets
              {'L3MON4D3/LuaSnip'},
              {'rafamadriz/friendly-snippets'},
            }
    }

    -- Autopair
    use { "windwp/nvim-autopairs", config = function() require("nvim-autopairs").setup {} end }

    -- Dashboard
    use { 'glepnir/dashboard-nvim' }
end)
