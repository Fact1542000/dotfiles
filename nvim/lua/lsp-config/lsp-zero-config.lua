local lsp = require('lsp-zero')

lsp.preset('recommended')

lsp.on_attach(function(client, bufnr)
    local noremap = { buffer = bufnr, remap = false }
    local bind = vim.keymap.set

    bind('n', '<leader>ca', '<cmd>lua vim.lsp.buf.code_action()<cr>', noremap)
    bind('n', '<leader>f', ':LspZeroFormat<cr>', noremap)
end)

lsp.setup()
