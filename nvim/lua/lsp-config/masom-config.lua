require("mason").setup()
require('mason-lspconfig').setup({
    ensure_installed = {
        'sumneko_lua',
        'omnisharp',
        'gopls',
        'emmet_ls',
        'dockerls',
    }
})
