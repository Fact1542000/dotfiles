local db = require('dashboard')

db.default_banner = {
  [[    ,---,                       ___                          ]],
  [[   '  .' \                    ,--.'|_                        ]],
  [[  /  ;    '.          ,---,   |  | :,'                       ]],
  [[ :  :       \     ,-+-. /  |  :  : ' :  .--.--.              ]],
  [[ :  |   /\   \   ,--.'|'   |.;__,'  /  /  /    '       .--,  ]],
  [[ |  :  ' ;.   : |   |  ,"' ||  |   |  |  :  /`./     /_ ./|  ]],
  [[ |  |  ;/  \   \|   | /  | |:__,'| :  |  :  ;_    , ' , ' :  ]],
  [[ '  :  | \  \ ,'|   | |  | |  '  : |__ \  \    `./___/ \: |  ]],
  [[ |  |  '  '--'  |   | |  |/   |  | '.'| `----.   \.  \  ' |  ]],
  [[ |  :  :        |   | |--'    ;  :    ;/  /`--'  / \  ;   :  ]],
  [[ |  | ,'        |   |/        |  ,   /'--'.     /   \  \  ;  ]],
  [[ `--''          '---'          ---`-'   `--'---'     :  \  \ ]],
  [[                                                      \  ' ; ]],
  [[                                                       `--`  ]],
}

db.custom_header = nil
db.custom_footer = {''}
db.custom_center = {
  a = {icon = '', desc = ' Find File    ', action = 'Telescope find_files'},
  b = {icon = '', desc = ' Search Text  ', action = 'Telescope live_grep'},
  c = {icon = '', desc = ' Recent Files ', action = 'Telescope oldfiles'},
  d = {icon = '', desc = ' Config       ', action = 'edit ~/.config/nvim/init.lua'}
}
